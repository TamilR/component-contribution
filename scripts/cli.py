"""Command line script for managing groups in equilibrator-cache."""
# The MIT License (MIT)
#
# Copyright (c) 2018 Institute for Molecular Systems Biology, ETH Zurich
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import logging
from path import Path

import click
import pandas as pd
import quilt
from equilibrator_cache import Compound, create_compound_cache_from_quilt
from equilibrator_cache.api import DEFAULT_QUILT_PKG as CACHE_QUILT_PKG
from equilibrator_cache.api import DEFAULT_QUILT_VERSION as CACHE_QUILT_VERSION
from equilibrator_cache.thermodynamic_constants import default_pMg, default_T
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from tqdm import tqdm

import click_log
from component_contribution import DEFAULT_QUILT_PKG as CC_QUILT_PKG

from component_contribution.training_data import FullTrainingData
from component_contribution.trainer import Trainer


logger = logging.getLogger()
click_log.basic_config(logger)
Session = sessionmaker()

DEFAULT_DATABASE_URL = "sqlite:///compounds.sqlite"


@click.group()
@click.help_option("--help", "-h")
@click_log.simple_verbosity_option(
    logger,
    default="INFO",
    show_default=True,
    type=click.Choice(["CRITICAL", "ERROR", "WARN", "INFO", "DEBUG"]),
)
def cli():
    """Command line interface to populate and update the equilibrator cache."""
    pass


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--quilt_pkg",
    metavar="PATH",
    default=CC_QUILT_PKG,
    show_default=True,
    help="Quilt package name for the compound cache.",
)
def init_quilt(quilt_pkg: str) -> None:
    """Calculate and store thermodynamic information for compounds."""
    from equilibrator_assets.groups import GroupsData

    quilt.build(quilt_pkg)
    cc_pkg = quilt.load(quilt_pkg)

    # first populate the quilt package with the training data and the
    # group_definitions table
    cc_pkg._add_group("data")
    for tab_name in [
        "formation_energies_transformed",
        "redox",
        "TECRDB",
        "toy_training_data",
        "group_definitions"
    ]:
        path = Path(__file__).parent.parent / "data" / (tab_name + ".csv")
        df = pd.read_csv(str(path))
        cc_pkg._set(["data", tab_name], df)

    # create a DataFrame with a summary of the groups. We only use this in
    # order to have the order of groups (i.e. names and some basic parameters)
    # when we want to analyze group-contribution results later on.
    groups = GroupsData.FromDataFrame(cc_pkg.data.group_definitions())
    group_df = groups.ToDataFrame()
    cc_pkg._set(["data", "group_summary"], group_df)

    # build and push the quilt package
    quilt.build(quilt_pkg, cc_pkg)
    quilt.push(quilt_pkg, is_public=True)


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--db-url",
    metavar="URL",
    default=DEFAULT_DATABASE_URL,
    show_default=True,
    help="A string interpreted as an rfc1738 compatible database URL.",
)
@click.option(
    "--ccache_quilt_pkg",
    metavar="PATH",
    default=CACHE_QUILT_PKG,
    show_default=True,
    help="Quilt package name for the compound cache.",
)
@click.option(
    "--ccache_quilt_version",
    metavar="VERSION",
    default=CACHE_QUILT_VERSION,
    show_default=True,
    help="Quilt package version for the compound cache.",
)
@click.option(
    "--ccache_quilt_tag",
    metavar="TAG",
    default=None,
    show_default=True,
    help="Quilt package tag for the compound cache.",
)
@click.option(
    "--batch-size",
    type=int,
    default=30000,
    show_default=True,
    help="The size of batches of compounds considered at a time.",
)
def decompose(
    db_url: str,
    ccache_quilt_pkg: str,
    ccache_quilt_version: str,
    ccache_quilt_tag: str,
    batch_size: int,
) -> None:
    """Calculate and store thermodynamic information for compounds."""

    from equilibrator_assets.group_decompose import (
        GroupDecomposer,
        GroupDecompositionError,
    )
    from equilibrator_assets.molecule import Molecule, OpenBabelError

    engine = create_engine(db_url)
    session = Session(bind=engine)

    # Query for all compounds.
    query = session.query(
        Compound.id, Compound.inchi_key, Compound.smiles
    ).filter(Compound.smiles.isnot(None), Compound.group_vector.is_(None))

    logger.debug("decomposing all compounds with structures")
    input_df = pd.read_sql_query(query.statement, query.session.bind)

    group_decomposer = GroupDecomposer()

    with tqdm(total=len(input_df), desc="Analyzed") as pbar:
        for index in range(0, len(input_df), batch_size):
            view = input_df.iloc[index : index + batch_size, :]
            compounds = []
            for row in view.itertuples(index=False):
                try:
                    # it is important to use here the SMILES representation
                    # of the molecule, and not the InChI, since it hold the
                    # information about the most abundant species at pH 7.
                    mol = Molecule.FromSmiles(row.smiles)
                    decomposition = group_decomposer.Decompose(
                        mol, ignore_protonations=False, raise_exception=True
                    )
                    group_vector = decomposition.AsVector()
                    compounds.append(
                        {"id": row.id, "group_vector": list(group_vector.flat)}
                    )
                    logger.debug(
                        "Decomposition of Compound(id=%d, inchi_key=%s): %r",
                        row.inchi_key,
                        row.id,
                        decomposition,
                    )
                except OpenBabelError as e:
                    compounds.append({"id": row.id, "group_vector": list()})
                    logger.debug(str(e))
                except GroupDecompositionError as e:
                    compounds.append({"id": row.id, "group_vector": list()})
                    logger.debug(str(e))

            session.bulk_update_mappings(Compound, compounds)
            session.commit()
            pbar.update(len(view))

    logger.debug("pushing DB changes to quilt (i.e. adding group vectors)")
    quilt.install(
        package=ccache_quilt_pkg,
        version=ccache_quilt_version,
        tag=ccache_quilt_tag,
        force=True
    )

    # rebuild the 'compounds' table, now with the new group vectors
    # compounds_df = pd.read_sql_table("compounds", engine)
    # quilt.build(join(quilt_pkg, "compounds", "compounds"), compounds_df)
    #
    # TODO: make sure to remove this commented code. Perhaps now
    #  that we write directly to the sqlite file stored inside quilt, the
    #  `build` operation is not necessary?

    quilt.push(ccache_quilt_pkg, is_public=True)


@cli.command()
@click.help_option("--help", "-h")
@click.option(
    "--quilt_pkg",
    metavar="PATH",
    default=CC_QUILT_PKG,
    show_default=True,
    help="Quilt package name for the component contribution model.",
)
@click.option(
    "--cache_quilt_version",
    default=CACHE_QUILT_VERSION,
    show_default=True,
    help="Quilt package version for the compound cache.",
)
@click.option(
    "--ignore_magnesium",
    is_flag=True,
    default=False,
    show_default=True,
    help="Use this flag if you want to ignore [Mg2+] ions during training.",
)
@click.option(
    "--ignore_temperature",
    is_flag=True,
    default=False,
    show_default=True,
    help="Use this flag if you want to ignore temperature during training.",
)
def train(
    quilt_pkg: str,
    cache_quilt_version: str,
    ignore_magnesium: bool,
    ignore_temperature: bool,
) -> None:
    """Train the Component Contribution model and push to quilt."""
    ccache = create_compound_cache_from_quilt(version=cache_quilt_version)
    train_data = FullTrainingData(
        ccache=ccache,
        package=quilt_pkg,
        override_p_mg=(default_pMg if ignore_magnesium else None),
        override_temperature=(default_T if ignore_temperature else None),
    )
    params = Trainer.train(train_data)

    cc_pkg = quilt.load(quilt_pkg)
    cc_pkg._add_group("parameters")
    for param_name, param_value in params._asdict().items():
        if param_value is not None:
            cc_pkg._set(["parameters", param_name], param_value)

    # build and push the quilt package
    quilt.build(quilt_pkg, cc_pkg)
    quilt.push(quilt_pkg, is_public=True)


if __name__ == "__main__":
    cli()
