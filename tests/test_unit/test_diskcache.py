"""unit test the diskcache of Zenodo-stored files."""
# The MIT License (MIT)
#
# Copyright (c) 2020 The Weizmann Institute of Science.
# Copyright (c) 2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import asyncio
import pandas as pd
import pytest
import pyzenodo3
from equilibrator_cache.zenodo import download_from_url, get_zenodo_files

from component_contribution import (
    ZENODO_DOI_GROUPS,
    ZENODO_DOI_PARAMETERS,
    ZENODO_DOI_TRAINING_DATA,
)
from component_contribution.diskcache import get_cached_file


@pytest.mark.parametrize(
    "doi",
    [ZENODO_DOI_GROUPS, ZENODO_DOI_PARAMETERS, ZENODO_DOI_TRAINING_DATA],
)
def test_zenodo(doi):
    """Test that pyzenodo is working properly."""
    zen = pyzenodo3.Zenodo()
    rec = zen.find_record_by_doi(doi)

    assert "files" in rec.data

    for file_dict in rec.data["files"]:
        assert "key" in file_dict
        assert "links" in file_dict
        assert "self" in file_dict["links"]


@pytest.mark.parametrize(
    "doi",
    [ZENODO_DOI_GROUPS],
)
def test_zenodo_download(doi):
    """Test downloading a dataframe from Zenodo."""
    zen = pyzenodo3.Zenodo()
    rec = zen.find_record_by_doi(doi)
    file_dict = rec.data["files"][0]
    assert file_dict["key"] == "group_definitions.csv"
    url = file_dict["links"]["self"]

    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    data = loop.run_until_complete(download_from_url(url))

    df = pd.read_csv(data)

    assert df.shape == (139, 8)


@pytest.mark.parametrize(
    "doi",
    [ZENODO_DOI_GROUPS],
)
def test_zenodo_download_async(doi):
    """Test downloading a dataframe from Zenodo."""
    filedict = get_zenodo_files(doi)
    df = pd.read_csv(filedict["group_definitions.csv"])

    assert df.shape == (139, 8)


@pytest.mark.parametrize(
    "doi, fname",
    [
        (ZENODO_DOI_GROUPS, "group_summary.csv"),
        (ZENODO_DOI_PARAMETERS, "cc_params.npz"),
        (ZENODO_DOI_TRAINING_DATA, "TECRDB.csv"),
    ],
)
def test_diskcache(doi, fname):
    """Test the diskcache directly."""
    get_cached_file(doi, fname)
