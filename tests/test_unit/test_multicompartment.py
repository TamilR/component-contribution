"""unit test for component-contribution predictions."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018-2020 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018-2020 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import pytest
from equilibrator_cache import Q_, CompoundCache, Reaction


@pytest.fixture(scope="module")
def cytoplasmic_half_reaction(ccache: CompoundCache) -> Reaction:
    """Create the cytoplasmic half of a reaction."""
    # formula = "bigg.metabolite:2oxoadp = bigg.metabolite:akg"
    formula = "bigg.metabolite:thf ="
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.fixture(scope="module")
def mitochondrial_half_reaction(ccache: CompoundCache) -> Reaction:
    """Create the mitochondrial half of a reaction."""
    formula = "= bigg.metabolite:thf"
    return Reaction.parse_formula(ccache.get_compound, formula)


@pytest.mark.parametrize(
    "p_h_1, p_h_2, ionic_strength_1, ionic_strength_2, "
    "temperature, delta_chi, p_mg_1, p_mg_2, "
    "exp_standard_dg_prime, exp_sigma",
    [(7.20, 8.00, 0.0, 0.0, 298.15, -155.0, 10.0, 10.0, -29.7, 0.0)],
)
def test_multicompartment_reaction(
    p_h_1,
    p_h_2,
    ionic_strength_1,
    ionic_strength_2,
    delta_chi,
    p_mg_1,
    p_mg_2,
    temperature,
    exp_standard_dg_prime,
    exp_sigma,
    cytoplasmic_half_reaction,
    mitochondrial_half_reaction,
    comp_contribution,
):
    """Test the standard dG prediction for a multicompartment reaction."""
    transported_protons = 21
    transported_charge = -2

    standard_dg_prime = comp_contribution.multicompartmental_standard_dg_prime(
        cytoplasmic_half_reaction,
        mitochondrial_half_reaction,
        transported_protons,
        transported_charge,
        Q_(p_h_1),
        Q_(p_h_2),
        Q_(ionic_strength_1, "M"),
        Q_(ionic_strength_2, "M"),
        Q_(delta_chi, "mV"),
        Q_(temperature, "K"),
        Q_(p_mg_1),
        Q_(p_mg_2),
    )

    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(
        exp_standard_dg_prime, abs=0.1
    )
    assert standard_dg_prime.error.m_as("kJ/mol") == pytest.approx(
        exp_sigma, abs=0.1
    )
