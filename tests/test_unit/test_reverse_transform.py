"""unit test for Legendre transforms."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import warnings

import pytest
from equilibrator_cache import Q_


warnings.filterwarnings("ignore", message="numpy.dtype size changed")


@pytest.mark.parametrize(
    "p_h, ionic_strength, temperature, p_mg, exp_ddg, reaction_name",
    [
        (6.0, 0.25, 298.15, 14.0, -36.9, "atpase"),
        (7.0, 0.25, 298.15, 14.0, -39.2, "atpase"),
        (8.0, 0.25, 298.15, 14.0, -44.1, "atpase"),
        (7.0, 0.25, 298.15, 1.0, -39.4, "atpase"),
        (7.0, 0.25, 298.15, 4.0, -38.5, "atpase"),
        (7.0, 0.25, 298.15, 7.0, -39.2, "atpase"),
    ],
)
def test_transform(
    p_h: float,
    ionic_strength: float,
    temperature: float,
    p_mg: float,
    exp_ddg: float,
    reaction_name: str,
    reaction_dict: dict,
):
    """Test the Legendre transform on a set of reactions and conditions."""
    reaction = reaction_dict[reaction_name]
    ddg = reaction.transform(
        p_h=Q_(p_h),
        ionic_strength=Q_(ionic_strength, "M"),
        temperature=Q_(temperature, "K"),
        p_mg=Q_(p_mg),
    )
    assert ddg.m_as("kJ/mol") == pytest.approx(exp_ddg, abs=0.1)
