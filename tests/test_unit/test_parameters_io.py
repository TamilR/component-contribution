"""unit test the I/O of the preprocessing parameters."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import pandas as pd
import pytest

from component_contribution.parameters import CCModelParameters


def test_npz():
    """Test the saving and loading functions for CCModelParameters."""
    params1 = CCModelParameters.from_zenodo()

    tmp_fname = "/tmp/cc_params.npz"
    params1.to_npz(tmp_fname)
    params2 = CCModelParameters.from_npz(tmp_fname)

    # compare the old and new params

    assert sorted(params1._fields) == sorted(params2._fields)

    for parameter_name in params1._fields:
        value1 = params1.__getattribute__(parameter_name)
        value2 = params2.__getattribute__(parameter_name)

        if type(value1) == pd.DataFrame:
            assert (value1.index == value2.index).all()
            assert (value1.columns == value2.columns).all()
            assert value1.values == pytest.approx(value2.values)
        else:
            assert value1 == pytest.approx(value2)
