"""unit test the training process of component-contribution."""
# The MIT License (MIT)
#
# Copyright (c) 2013 The Weizmann Institute of Science.
# Copyright (c) 2018 Novo Nordisk Foundation Center for Biosustainability,
# Technical University of Denmark.
# Copyright (c) 2018 Institute for Molecular Systems Biology,
# ETH Zurich, Switzerland.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


import warnings

import pytest
from equilibrator_cache import Q_, CompoundCache

from component_contribution.predict import GibbsEnergyPredictor
from component_contribution.trainer import Trainer
from component_contribution.training_data import TrainingData


warnings.filterwarnings("ignore", message="numpy.dtype size changed")


@pytest.fixture(scope="module")
def comp_contribution(
    toy_training_data: TrainingData, ccache: CompoundCache
) -> GibbsEnergyPredictor:
    """Create a GibbsEnergyPredictor object."""
    params = Trainer.train(toy_training_data)
    return GibbsEnergyPredictor(parameters=params)


def test_train(comp_contribution):
    """Test the training process in terms of matrix dimensions."""
    n_groups = 163
    n_compounds = 96
    n_non_decomposable = 6
    n_reactions = 48
    n_groups_extended = n_groups + n_non_decomposable

    assert comp_contribution.preprocess.Ng == n_groups  # number of real groups
    p = comp_contribution.params

    assert p.train_b.shape == (n_reactions,)
    assert p.train_S.shape == (n_compounds, n_reactions)
    assert p.train_G.shape == (n_compounds, n_groups_extended)
    assert p.train_w.shape == (n_reactions,)
    assert p.inv_GS.shape == (n_reactions, n_groups_extended)
    assert p.dG0_rc.shape == (n_compounds,)
    assert p.dG0_cc.shape == (n_compounds,)
    assert p.dG0_gc.shape == (n_groups_extended,)


def test_standard_dg_calculation_atp(comp_contribution, reaction_dict):
    """Test the trained CC model on ATP hydrolysis."""
    atpase_reaction = reaction_dict["atpase"]
    standard_dg = comp_contribution.standard_dg(atpase_reaction)
    assert standard_dg.value.m_as("kJ/mol") == pytest.approx(11.6, abs=0.1)
    assert standard_dg.error.m_as("kJ/mol") == pytest.approx(1.1, abs=0.1)


@pytest.mark.parametrize(
    "p_h, ionic_strength, temperature, p_mg, exp_standard_dg_prime, "
    "exp_sigma, reaction_name",
    [
        (6.00, 0.25, 298.15, 10.0, -25.3, 1.1, "atpase"),
        (7.00, 0.25, 298.15, 10.0, -27.6, 1.1, "atpase"),
        (8.00, 0.25, 298.15, 10.0, -32.5, 1.1, "atpase"),
        (6.00, 0.25, 298.15, 10.0, 6.9, 0.7, "transadenylate"),
        (7.50, 0.25, 298.15, 10.0, 3.2, 0.7, "transadenylate"),
        (8.00, 0.25, 298.15, 10.0, 2.9, 0.7, "transadenylate"),
        (6.02, 0.25, 298.15, 1.96, -13.0, 0.7, "hexokinase"),
        (7.00, 0.25, 311.15, 3.00, -13.4, 0.5, "adenosine_kinase"),
        (8.86, 0.25, 298.15, 4.50, -12.9, 0.5, "adenosine_kinase"),
    ],
)
def test_standard_dg_prime_calculation_atp(
    p_h: float,
    ionic_strength: float,
    temperature: float,
    p_mg: float,
    exp_standard_dg_prime: float,
    exp_sigma: float,
    reaction_name: str,
    reaction_dict: dict,
    comp_contribution: GibbsEnergyPredictor,
):
    """Test the trained CC model on ATP hydrolysis in different pH levels.

    This test includes the effect of [Mg2+] ions both in the reverse transform
    and in the prediction procedure.
    """
    reaction = reaction_dict[reaction_name]
    standard_dg_prime = comp_contribution.standard_dg_prime(
        reaction,
        p_h=Q_(p_h),
        ionic_strength=Q_(ionic_strength, "M"),
        temperature=Q_(temperature, "K"),
        p_mg=Q_(p_mg),
    )
    assert standard_dg_prime.value.m_as("kJ/mol") == pytest.approx(
        exp_standard_dg_prime, abs=0.1
    )
    assert standard_dg_prime.error.m_as("kJ/mol") == pytest.approx(
        exp_sigma, abs=0.1
    )
